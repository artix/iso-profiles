# iso-profiles

The Artix ISO profiles.


The profiles are designated base, netinstall, minimal plasma.

Base only offers a bare minimum system, installable from the CLI. For advanced users.

Minimal plasma profile (comes with a basic DE, slightly preconfigured. The graphical installer of choice is Calamares. For knowledgeable users.

Netinstall profile has the same minimal plasma live DE, but the calamares installer is configured in pure online netinstall mode and has almost every package selectable, including the init system to install. Everything is downloaded instead of the offline unpack mode, including the init system to install. Full customization of the target system.

Common settings among all profiles are stored in ''common''.
